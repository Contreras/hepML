## Bayesian optimization based on Gaussian process regression search (controlling the exploration-exploitation trade-off)

# ---- Import common python libraries
import sys
import time
import numpy as np
import pandas as pd
import random
import collections

# ---- Import from root_numpy library 
#import root_numpy
#from root_numpy import root2array, rec2array

# ---- Import from root_pandas library
#import root_pandas
#from root_pandas import read_root

# ----- Import scikit-learn
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.wrappers.scikit_learn import KerasClassifier

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations

# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)


# ---- Data loading function
def load(sig_filename, bkg_filename, category, features):
    """Data loader.
    
    Parameters
    ----------
    sig_filename : array, shape = [n_samples]
            true class, intergers in [0, n_classes - 1)
    bkg_filename : array, shape = [n_samples, n_classes]
    category: string
    features: array, shape = [n_features]

    Returns
    -------
    data : pandas.DataFrame
    """
   
    signal = read_root([sig_filename], category, columns=features)
    background = read_root([bkg_filename], category, columns=features)
    
    signal['y'] = 1.
    background['y'] = 0.
      
    # for sklearn data is usually organised
    # into one 2D array of shape (n_samples x n_features)
    # containing all the data and one array of categories
    # of length n_samples
    data = pd.concat([signal, background])

    return data


# ---- Feature names
branch_names = """mass_tag_tag_min_deltaR,median_mass_jet_jet,
    maxDeltaEta_tag_tag,mass_higgsLikeDijet,HT_tags,
    btagDiscriminatorAverage_tagged,mass_jet_tag_min_deltaR,
    mass_jet_jet_min_deltaR,mass_tag_tag_max_mass,maxDeltaEta_jet_jet,
    centrality_jets_leps,centrality_tags,globalTimesEventWeight""".split(",")

features = [c.strip() for c in branch_names]
features = (b.replace(" ", "_") for b in features)
features = list(b.replace("-", "_") for b in features)

# --- Load dataset
signal_sample     = "data/signalMC.root"
background_sample = "data/backgroundMC.root"
tree_category     = "event_mvaVariables_step7_cate4"

data = load(signal_sample, background_sample, tree_category, features)

print "Total number of events: {}\nNumber of features: {}".format(data.shape[0], data.shape[1])

# ---- Create features dataframe and target array
data_X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
data_y = data["y"]
input_dim = data_X.shape[1]

# ---- Split dataset
#X_train, X_test, Y_train, Y_test = train_test_split(data_X, data_y, test_size=0.33, random_state=seed)


# ---- Handles objective function
class ObjectiveClass:
    
    # class constuctor
    def __init__(self, X, y, n_folds=3, scoring='roc_auc', n_jobs=1):
        # Split development set into a train and test set
        self.X = X
        self.y = y
        self.seed = 42
        self.n_folds= n_folds
        self.cv = KFold(n_splits=self.n_folds, shuffle=True, random_state=self.seed)
        self.scoring = scoring
        self.n_jobs = n_jobs
 
    # objective function we want to minimize 
    def __call__(self, values):
        # Build paramter list with updated values
        params_dict = dict(zip(self.keys, values))

        # Optimize hyper-parameters of classifier
        self.clf.set_params(**params_dict)

        # Cross-validation mean absolute error of a classifier as a function of its hyper-parameters
        score = cross_val_score(self.clf, self.X, self.y, cv=self.cv, scoring=self.scoring, n_jobs=self.n_jobs).mean()
        return 1-score #-score
    
    def setEstimator(self, Classifier):
        self.clf = Classifier
        
    def paramKeys(self, keys):
        self.keys = keys


# ---- Set deep neural network architecture
def create_model(nlayers=1, nneurons=300, dropout_rate=0.0,
                 l2_norm=0.001, activation='relu', init_mode='lecun_normal',
                 learning_rate=1e-3, save_as='model.h5', input_dim=12):

    # prevent from giving float value
    nlayers = int(nlayers)
    nneurons = int(nneurons)

    # create model
    model = Sequential()

    # indicate the number of layers
    model.add(Dense(nneurons,
                        input_dim=input_dim,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
    model.add(Dropout(dropout_rate))

    for layer in range(nlayers-1):
        model.add(Dense(nneurons,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
        model.add(Dropout(dropout_rate))

    # output layer
    model.add(Dense(2, kernel_initializer=init_mode, activation='softmax',
                        kernel_regularizer=l2(l2_norm)))
    #model.add(Dense(1, kernel_initializer=kernel_initializer,
    #          activation='sigmoid', kernel_regularizer=l2(l2_norm)))
    
    # compile model (set loss and optimize)
    model.compile(loss='categorical_crossentropy', 
                  optimizer=Adam(lr=learning_rate), metrics=['accuracy'])
    # Store model to file
    model.save(save_as)

    # print summary report
    model.summary()

    return model

# ---- Set configuration space
parameters = collections.OrderedDict(
    [
        #('kerasclassifier__input_dim', (6, input_dim)),
        ('kerasclassifier__nlayers', (1, 3)),
        ('kerasclassifier__nneurons', (150, 300)),
        ('kerasclassifier__l2_norm', (0.01, 0.1)), 
        ('kerasclassifier__dropout_rate', (0.01, 0.1))
    ])

# ---- Preprocessing using 0-1 scaling by removing the mean and scaling to unit variance 
scaler = RobustScaler()

# ---- Create model for use in scikit-learn
pipe_classifiers = {
    'keras':  make_pipeline(scaler,  KerasClassifier(build_fn=create_model, batch_size=128, epochs=50, verbose=0))
}

# ---- Instantiate objective function to optimize
objective = ObjectiveClass(data_X.values, data_y.values)
objective.setEstimator(pipe_classifiers['keras'])
objective.paramKeys(parameters.keys())

# ---- Bayesian optimization (basedon on Gaussian Process regression search)
process = time.clock()
clf_gp_ei = gp_minimize(func=objective,                   # the function to minimize
                        dimensions=parameters.values(),   # the bounds on each dimension of the optimization space
                        acq_func="EI",                    # the acquisition function ("EI", "LCB", "PI")
                        n_calls=30,                       # the number of evaluations of the objective function
                        random_state=0,                   # the random seed
                        n_jobs=1)                         # the number of threads to use
print("Optimization time: %.3f" % (time.clock()-process))
print("Best score=%.4f (EI)" % clf_gp_ei.fun)

print("""Expected Improvement (EI) best parameters:
- nlayers= %s
- nneurons= %s
- dropout_rate= %s
- l2_norm= %s""" % (str(clf_gp_ei.x[0]), str(clf_gp_ei.x[1]), 
                    str(clf_gp_ei.x[2]), str(clf_gp_ei.x[3])))

