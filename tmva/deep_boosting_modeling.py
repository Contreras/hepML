## DNN and BDT modeling

# ---- Import common python libraries
import sys
import time

# ---- Import libraries
import ROOT
from ROOT import TMVA

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations


# ---- Select set of variables to use in MVA modeling
def get_variables(set="dl"):
    vars = {
        "dl" : ["pT_jet_tag_min_deltaR", "multiplicity_higgsLikeDijet15", "maxDeltaEta_tag_tag",
              "centrality_tags","mass_tag_tag_max_mass","H3_tag","btagDiscriminatorAverage_tagged",
              "btagDiscriminatorAverage_untagged","maxDeltaEta_jet_jet","mass_tag_tag_min_deltaR"]
    }
    return vars[set]

# ---- Set deep neural network architecture
def create_model(nlayers=1, nneurons=300, dropout_rate=0.0,
                 l2_norm=0.001, activation='relu', init_mode='lecun_normal',
                 learning_rate=1e-3, save_as='model.h5', input_dim=12):

    # prevent from giving float value
    nlayers = int(nlayers)
    nneurons = int(nneurons)

    # create model
    model = Sequential()

    # indicate the number of layers
    model.add(Dense(nneurons,
                        input_dim=input_dim,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
    model.add(Dropout(dropout_rate))

    for layer in range(nlayers-1):
        model.add(Dense(nneurons,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
        model.add(Dropout(dropout_rate))

    # output layer
    model.add(Dense(2, kernel_initializer=init_mode, activation='softmax',
                        kernel_regularizer=l2(l2_norm)))
    #model.add(Dense(1, kernel_initializer=kernel_initializer,
    #          activation='sigmoid', kernel_regularizer=l2(l2_norm)))
    
    # compile model (set loss and optimize)
    model.compile(loss='categorical_crossentropy', 
                  optimizer=Adam(lr=learning_rate), metrics=['accuracy'])

    # store model to file
    model.save(save_as)

    # print summary report
    model.summary()

    return model



# ---- Choose TMVA method to train and evaluate 
methodBaseline    = "BDT"
methodAlternative = "PyKeras"

# ---- Create a ROOT output file where TMVA will store ntuples, histograms, etc. 
outfileName = "ttH_TMVA_Keras_test.root" #"ttH_TMVA_Keras_" + str(sys.argv[0]) + ".root"
outputFile  = ROOT.TFile( outfileName, "RECREATE" )

# ---- Select set of variables via dictionary key
VARSET = "dl" #str(sys.argv[1])  

# ---- Provide Input file
path_to_training_trees = "data/" #"combined/"
sigSrcTrain     = ROOT.TFile( path_to_training_trees + "signalTraining.root" )
bkgSrcTrain     = ROOT.TFile( path_to_training_trees + "backgroundTraining.root" )
sigSrcTest      = ROOT.TFile( path_to_training_trees + "signalTesting.root" )
bkgSrcTest      = ROOT.TFile( path_to_training_trees + "backgroundTesting.root" )

tree = "event_mvaVariables_step7_cate3"
sigTreeTrain = sigSrcTrain.Get( tree )
bkgTreeTrain = bkgSrcTrain.Get( tree )
sigTreeTest  = sigSrcTest.Get(  tree )
bkgTreeTest  = bkgSrcTest.Get(  tree )

print("--- TMVAClassification       : Using input sigSrcTrain file:\n %s"   % (sigSrcTrain.GetName()))
print("--- TMVAClassification       : Using input bkgSrcTrain file:\n %s"   % (bkgSrcTrain.GetName()))
print("--- TMVAClassification       : Using input sigSrcTest file:\n %s"    % (sigSrcTest.GetName()))
print("--- TMVAClassification       : Using input bkgSrcTest file:\n %s"  % (bkgSrcTest.GetName()))

# ---- This loads the library
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()

# ---- Declare TMVA Factory
factory = TMVA.Factory( "TMVAClassification", outputFile,
                        "!V:Color:DrawProgressBar:Transformations=I:AnalysisType=Classification" )

# ---- Declare DataLoader
loader = TMVA.DataLoader( "ttH_dataset" );

# ---- Add the feature variables, names reference branches in inputFile ttree
for var in get_variables( VARSET ):
    loader.AddVariable( var )

 ROOT.TMVA.gConfig().SetDrawProgressBar( False );

# ---- Global event weights per tree
sigWeight   = 1.0
bkgWeight   = 1.0;

# ---- Apply additional cuts on the signal and background samples
mycuts = ROOT.TCut()
mycutb = ROOT.TCut()

# ---- Register the trees
loader.AddSignalTree    ( sigTreeTrain, sigWeight, TMVA.Types.kTraining );
loader.AddBackgroundTree( bkgTreeTrain, bkgWeight, TMVA.Types.kTraining );
loader.AddSignalTree    ( sigTreeTest,  sigWeight, TMVA.Types.kTesting  );
loader.AddBackgroundTree( bkgTreeTest,  bkgWeight, TMVA.Types.kTesting  );

# ----- Set training and testing configiration
dataString = "nTrain_Signal=0:nTrain_Background=0:nTest_Signal=0:nTest_Background=0:SplitMode=Random:NormMode=EqualNumEvents:!V"
loader.PrepareTrainingAndTestTree( mycuts, mycutb, dataString )

# ----- Apply hyper-parameter obtained from SkOpt tunning library
create_model( input_dim=len(get_variables(VARSET)),
              nlayers=1, nneurons=300, dropout_rate= 0.01,
              l2_norm=0.01, learning_rate=1e-3,
              save_as='model.h5' )

# ---- Book methods
factory.BookMethod( loader, TMVA.Types.kPyKeras, methodAlternative,
            "H:!V:VarTransform=G:FilenameModel=model.h5:BatchSize=128:NumEpochs=50" )

factory.BookMethod( loader, TMVA.Types.kBDT, methodBaseline, "!H:!V:NTrees=638:BoostType=Grad:Shrinkage=0.00603:UseBaggedBoost:GradBaggingFraction=0.41047:nCuts=42:MaxDepth=2:NegWeightTreatment=IgnoreNegWeightsInTraining" )

#---- Train MVAs using the set of training events
factory.TrainAllMethods()

# ---- Evaluate all MVAs using the set of test events
factory.TestAllMethods()

# ----- Evaluate and compare performance of all configured MVAs
factory.EvaluateAllMethods()

# Save the output
outputFile.Close()

print("==> Wrote root file: %s" % (outputFile.GetName()))
print("==> TMVAClassification is done!")

