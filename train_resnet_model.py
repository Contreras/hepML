# SOURCE: https://github.com/raghakot/keras-resnet/blob/master/tests/test_resnet.py
"""
Train ResNet-34 on the small images dataset.
GPU run command with TensorFlow backend, the GPU is automatically used:
"""
# ---- Basic library imports
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from time import time

# ---- Import keras library
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping
from keras.utils import plot_model, np_utils

# ---- Import data loader
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel
from neural_network.models import ResnetBuilder

# ---- Import Scikit-learn library
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score

# ---- Declare sample and features to use
signal_sample     = 'data/signal.root'
background_sample = 'data/background.root'
treename          = 'event_mvaVariables_step7_cate4'

features          = [
    'lepton_1_px', 'lepton_1_py', 'lepton_1_pz',
    'lepton_2_px', 'lepton_2_py', 'lepton_2_pz',
    'jet_1_px', 'jet_1_py', 'jet_1_pz', #'jet_1_btag_discrim', #'jet_1_pe',
    'jet_2_px', 'jet_2_py', 'jet_2_pz', #'jet_2_btag_discrim', #'jet_2_pe',
    'jet_3_px', 'jet_3_py', 'jet_3_pz', #'jet_3_btag_discrim', #'jet_3_pe',
    'jet_4_px', 'jet_4_py', 'jet_4_pz', #'jet_4_btag_discrim', #'jet_4_pe',
    'jet_5_px', 'jet_5_py', 'jet_5_pz', #'jet_5_btag_discrim', #'jet_5_pe',
    'jet_6_px', 'jet_6_py', 'jet_6_pz', #'jet_6_btag_discrim'  #'jet_6_pe',
    ]

verbose = 2

# ---- Input image dimensions (use 8 x 8 pixel image)
img_channels = 2
img_depth, img_width, img_height = img_channels, 8, 8 #img_rows, img_cols

# ---- Set fitting parameters
batch_size = 256
nb_classes = 2
epochs     = 80
data_augmentation = True

# ---- Image input data is expressed as a 3-dim matrix of channels x width x height
input_shape = (img_depth, img_width, img_height)

# ---- Load the analysis dataset
dataloader =  DataLoader(signal_sample, background_sample, treename, features)
print("Total number of events: {}\nNumber of features (incl. class label): {}\n".format(dataloader.data.shape[0],
                                                                                        dataloader.data.shape[1]))

# ---- Scikit-learn style dataset format from pandas dataframe (example here is sampling from dataset)
frac=1. #1e-2
sampled_data = dataloader.data#.sample(frac=frac, replace=False, random_state=42)

X = sampled_data.drop(['y'], axis=1, inplace=False)
y = sampled_data['y']

# ---- A standard split of the dataset is used to evaluate and compare models
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10, random_state=42)

# ---- Create two-dim image training & test dataset
start = time()
X_train_jet_img = dataloader.create_image(X_train, 'jet', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_jet_img  = dataloader.create_image(X_test,  'jet', input_shape)

start = time()
X_train_lepton_img = dataloader.create_image(X_train, 'lepton', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_lepton_img  = dataloader.create_image(X_test, 'lepton', input_shape)

X_train_jet_img    = X_train_jet_img.reshape(X_train_jet_img.shape[0], X_train_jet_img.shape[2], X_train_jet_img.shape[3], X_train_jet_img.shape[1])
X_test_jet_img     = X_test_jet_img.reshape(X_test_jet_img.shape[0], X_test_jet_img.shape[2], X_test_jet_img.shape[3], X_test_jet_img.shape[1])
X_train_lepton_img = X_train_lepton_img.reshape(X_train_lepton_img.shape[0], X_train_lepton_img.shape[2], X_train_lepton_img.shape[3], X_train_lepton_img.shape[1])
X_test_lepton_img  = X_test_lepton_img.reshape(X_test_lepton_img.shape[0], X_test_lepton_img.shape[2], X_test_lepton_img.shape[3], X_test_lepton_img.shape[1])

# ---- Concatenate the various imagine depth (use axis=1 since reshape is already applied)
Images_array_train = np.concatenate((X_train_jet_img, X_train_lepton_img), axis=3)
Images_array_test  = np.concatenate((X_test_jet_img,  X_test_lepton_img),  axis=3)

# ---- Convert class vectors to binary class matrices
y_train = np_utils.to_categorical(y_train, nb_classes)
y_test = np_utils.to_categorical(y_test, nb_classes)

# ---- Set model training monitoring options
lr_reducer    = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.001, patience=10)
csv_logger    = CSVLogger('resnet34_logger.csv')

# ---- Build Residual network model (type 34)
resnet_model = ResnetBuilder.build_resnet_34((img_channels, img_height, img_width), nb_classes)
resnet_model.compile(loss='categorical_crossentropy',
                     optimizer='adam',
                     metrics=['accuracy'])

if not data_augmentation:
    print('Not using data augmentation.')
    resnet_model_history = resnet_model.fit(Images_array_train, y_train,
                                            batch_size=batch_size,
                                            epochs=epochs,
                                            validation_data=(Images_array_test, y_test),
                                            shuffle=True,
                                            callbacks=[lr_reducer,
                                                       csv_logger,
                                                       #early_stopper
                                                   ])
else:
    print('Using real-time data augmentation.')
    # This will do preprocessing and realtime data augmentation:
    datagen = ImageDataGenerator(
        featurewise_center=False,             # set input mean to 0 over the dataset
        samplewise_center=False,              # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,   # divide each input by its std
        zca_whitening=False,                  # apply ZCA whitening
        rotation_range=0,                     # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,                # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,               # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,                 # randomly flip images
        vertical_flip=False)                  # randomly flip images

    # Compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied).
    datagen.fit(Images_array_train)

    # Fit the model on the batches generated by datagen.flow().
    start = time()
    resnet_model_history = resnet_model.fit_generator(datagen.flow(Images_array_train, y_train, batch_size=batch_size),
                                                      steps_per_epoch=Images_array_train.shape[0] // batch_size,
                                                      validation_data=(Images_array_test, y_test),
                                                      epochs=epochs, verbose=1, max_q_size=100,
                                                      callbacks=[lr_reducer, early_stopper, csv_logger])
    print("Model training took %.2f seconds." % (time() - start))

# ---- Evaluate model test performance
loss, accuracy = resnet_model.evaluate(Images_array_test, y_test, verbose=verbose)
print('\nEvaluate test loss \n%s: %.2f%%'    % (resnet_model.metrics_names[0], loss*100))
print('Evaluate test accuracy \n%s: %.2f%%'  % (resnet_model.metrics_names[1], accuracy*100))

# ---- Evaluate model training performance 
loss, accuracy = resnet_model.evaluate(Images_array_train, y_train, verbose=verbose)
print('Evaluate train loss \n%s: %.2f%%'     % (resnet_model.metrics_names[0], loss*100))
print('Evaluate train accuracy \n%s: %.2f%%' % (resnet_model.metrics_names[1], accuracy*100))

# ---- Store network achitecture diagram into png
plot_model(resnet_model, to_file='plots/model_resnet.pdf')

# ---- List all data in history
print(resnet_model_history.history.keys())

## Visualize Model Training History

# ---- Summarize history for accuracy
plt.plot(resnet_model_history.history['acc'])
plt.plot(resnet_model_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Summarize history for loss
plt.plot(resnet_model_history.history['loss'])
plt.plot(resnet_model_history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Calculate AUC of ROC
predictions = resnet_model.predict(Images_array_test)
predictions = [item[1] for item in predictions.tolist()]
y_test      = [item[1] for item in y_test.tolist()]

fpr, tpr, _ = roc_curve(y_test, predictions)
roc_auc = auc(fpr, tpr)

# Plot all ROC curves
plt.plot(fpr, tpr, lw=2, label='%s (AUC = %0.3f)'%('ResNet version', roc_auc))
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title("Receiver operating characteristic curve (%s events)" % (len(y_test)))
leg = plt.legend(loc="best", frameon=True, fancybox=True, fontsize=8)
leg.get_frame().set_edgecolor('w')
frame = leg.get_frame()
frame.set_facecolor('White')
print('AUC: %f' % roc_auc)
plt.show()
