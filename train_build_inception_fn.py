# Source: https://becominghuman.ai/understanding-and-coding-inception-module-in-keras-eb56e9056b4b

## Create a Convolutional Neural Network with Keras
from __future__ import print_function

# ---- Import basic libraries
import matplotlib.pyplot as plt
import numpy as np
from time import time

# ---- Import Keras library
import keras
from keras.models import model_from_json
from keras.utils import plot_model
from keras.utils import multi_gpu_model
from keras.callbacks import EarlyStopping
from keras.layers import Input

# Standardize images across the dataset, mean=0, stdev=1
from keras.preprocessing.image import ImageDataGenerator

# ---- Import Scikit-learn library
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score

# ---- Import data loader
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel

# ---- Import plotter
from visualization.plotter import Plotter


# ---- Declare sample and features to use
signal_sample     = 'data/signal.root'
background_sample = 'data/background.root'
treename          = 'event_mvaVariables_step7_cate4'

features          = [
    'jet_1_px', 'jet_1_py', 'jet_1_pz', 
    'jet_2_px', 'jet_2_py', 'jet_2_pz', 
    'jet_3_px', 'jet_3_py', 'jet_3_pz', 
    'jet_4_px', 'jet_4_py', 'jet_4_pz', 
    'jet_5_px', 'jet_5_py', 'jet_5_pz',
    'jet_6_px', 'jet_6_py', 'jet_6_pz',
    ]

# ---- Dimensions of our images (use 8 x 8 pixel image)
channel = 1
img_depth, img_width, img_height = channel, 8, 8

# ---- Image input data is expressed as a 3-dime matrix of channels x width x height
input_shape = (img_depth, img_width, img_height)
input_img = Input(shape=input_shape)
verbose = 2

# ---- Load the analysis dataset 
dataloader =  DataLoader(signal_sample, background_sample, treename, features)
print("Total number of events: {}\nNumber of features (incl. class label): {}\n".format(dataloader.data.shape[0],
                                                                                        dataloader.data.shape[1]))
# ---- Scikit-learn style dataset format from pandas dataframe (example here is sampling from dataset)
data = dataloader.data
X = data.drop(['y'], axis=1, inplace=False)
labels = data['y']

# ---- Convert labels to categorical one-hot encoding
y = keras.utils.to_categorical(labels, num_classes=2)

# ---- A standard split of the dataset is used to evaluate and compare models
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)

# ---- Create two-dim image training & test dataset
X_train_jet_img = dataloader.create_image(X_train, 'jet', input_shape)
X_test_jet_img  = dataloader.create_image(X_test,  'jet', input_shape)

# ---- Set deep neural network architecture: [m input] -> [n neurons] -> [1 output]
cnn = DeepModel(input_shape=input_shape)
inception_model = cnn.build_inception_fn(input_img=input_img)

# ---- Use early stopping on training when the validation loss isn't decreasing anymore
early_stopping = EarlyStopping(monitor='val_loss', patience=5)

# ---- Store network architecture
plot_model(inception_model, to_file='plots/build_inception_fn.pdf', show_shapes=True, show_layer_names=True)

# ---- Fit model on training data (use 10% of data for model validation)
start = time()
inception_model_history = inception_model.fit(X_train_jet_img, y_train, validation_split=0.20,
                                              batch_size=256, epochs=80, verbose=verbose,
                                              callbacks=[early_stopping]
                                          )
print("Model training took %.2f seconds." % (time() - start))


# ---- List all data in history
print(inception_model_history.history.keys())

## Visualize Model Training History
# ---- Summarize history for accuracy
plt.plot(inception_model_history.history['acc'])
plt.plot(inception_model_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Summarize history for loss
plt.plot(inception_model_history.history['loss'])
plt.plot(inception_model_history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Calculate AUC of ROC
predictions = inception_model.predict(X_test_jet_img)

# ---- Must applying transformation as it does not support multi-label objects
predictions = [item[0] for item in predictions]
y_test = [item[0] for item in y_test]

fpr, tpr, _ = roc_curve(y_test, predictions)
roc_auc = auc(fpr, tpr)

# Plot all ROC curves
plt.plot(fpr, tpr, lw=2, label='%s (AUC = %0.3f)'%('Inception CNN', roc_auc))
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title("Receiver operating characteristic curve")
leg = plt.legend(loc="best", frameon=True, fancybox=True, fontsize=8)
leg.get_frame().set_edgecolor('w')
frame = leg.get_frame()
frame.set_facecolor('White')
print('AUC: %f' % roc_auc)
plt.show()
