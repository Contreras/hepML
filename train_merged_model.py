# ---- Import common python libraries
from __future__ import print_function
import sys
from time import time
import numpy as np
import pandas as pd
import random
import collections

# ---- Import from root_numpy library 
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# ---- Import from matplotlib
import matplotlib.pyplot as plt

# ----- Import scikit-learn
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve, auc, roc_auc_score

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils.vis_utils import plot_model
from keras.layers import Dense, Dropout, Activation, Flatten, Convolution2D, MaxPooling2D, merge
from keras.layers.merge import Concatenate
from keras.callbacks import EarlyStopping
from keras.layers import Input

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations

# ---- Import data loader library
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel

# ---- Import optimization for hyper-parameter
from neural_network.optimization import SkOptObjective, CrossValidation

# ---- Import plotter library
from visualization.plotter import Plotter

# ---- Import Imbalanced-learn library
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import SMOTE
from imblearn.pipeline import Pipeline 
from imblearn.pipeline import make_pipeline as make_imb_pipeline

# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)


# --- Set filenames
signal_sample     = "data/signal.root"
background_sample = "data/background.root"
treename          = "event_mvaVariables_step7_cate4"

# ---- Feature names
features_dnn = [
    "mass_tag_tag_min_deltaR", "median_mass_jet_jet",
    "maxDeltaEta_tag_tag", "mass_higgsLikeDijet", "HT_tags",
    "btagDiscriminatorAverage_tagged", "mass_jet_tag_min_deltaR",
    "mass_jet_jet_min_deltaR", "mass_tag_tag_max_mass", "maxDeltaEta_jet_jet",
    "centrality_jets_leps", "centrality_tags", #"globalTimesEventWeight"
]

features_cnn = [
    'lepton_1_px', 'lepton_1_py', 'lepton_1_pz',
    'lepton_2_px', 'lepton_2_py', 'lepton_2_pz',
    'jet_1_px', 'jet_1_py', 'jet_1_pz', 'jet_1_btag_discrim', #'jet_1_pe',
    'jet_2_px', 'jet_2_py', 'jet_2_pz', 'jet_2_btag_discrim', #'jet_2_pe',
    'jet_3_px', 'jet_3_py', 'jet_3_pz', 'jet_3_btag_discrim', #'jet_3_pe',
    'jet_4_px', 'jet_4_py', 'jet_4_pz', 'jet_4_btag_discrim', #'jet_4_pe',
    'jet_5_px', 'jet_5_py', 'jet_5_pz', 'jet_5_btag_discrim', #'jet_5_pe',
    'jet_6_px', 'jet_6_py', 'jet_6_pz', 'jet_6_btag_discrim'  #'jet_6_pe',
    ]

verbose = 2

# --- Load dataset
dataloader_dnn = DataLoader(signal_sample, background_sample, treename, features_dnn)
data_dnn = dataloader_dnn.data
print("Total number of events: {}\nNumber of features: {}\n".format(data_dnn.shape[0], data_dnn.shape[1]))

dataloader_cnn = DataLoader(signal_sample, background_sample, treename, features_cnn)
data_cnn = dataloader_cnn.data
print("Total number of events: {}\nNumber of features: {}\n".format(data_cnn.shape[0], data_cnn.shape[1]))

# ---- If jet b-tag discriminant value is >= medium WP tag as jet as b-tagged else set jet content to zero 
def labels(v):
    if v.iloc[1] >= 0.8484: # medium WP
        return v.iloc[0]
    else:
        return 0.

# ---- Set b-tag jet dataframe column
dataloader_cnn.data['bjet_1_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_1_px','jet_1_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_1_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_1_py','jet_1_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_1_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_1_pz','jet_1_btag_discrim']]), axis=1)

dataloader_cnn.data['bjet_2_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_2_px','jet_2_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_2_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_2_py','jet_2_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_2_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_2_pz','jet_2_btag_discrim']]), axis=1)

dataloader_cnn.data['bjet_3_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_3_px','jet_3_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_3_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_3_py','jet_3_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_3_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_3_pz','jet_3_btag_discrim']]), axis=1)

dataloader_cnn.data['bjet_4_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_4_px','jet_4_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_4_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_4_py','jet_4_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_4_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_4_pz','jet_4_btag_discrim']]), axis=1)

dataloader_cnn.data['bjet_5_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_5_px','jet_5_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_5_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_5_py','jet_5_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_5_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_5_pz','jet_5_btag_discrim']]), axis=1)

dataloader_cnn.data['bjet_6_px'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_6_px','jet_6_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_6_py'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_6_py','jet_6_btag_discrim']]), axis=1)
dataloader_cnn.data['bjet_6_pz'] = dataloader_cnn.data.apply(lambda row: labels(row[['jet_6_pz','jet_6_btag_discrim']]), axis=1)

# ---- Create features dataframe and target array
X_dnn = data_dnn.drop(["y"], axis=1, inplace=False)
y_dnn = data_dnn["y"]
input_dim = X_dnn.shape[1]

# ---- Split dataset
X_dnn_train, X_dnn_test, y_dnn_train, y_dnn_test = train_test_split(X_dnn, y_dnn, test_size=0.20, random_state=seed)

# ---- Dimensions of our images (use 8 x 8 pixel image)
channel = 2
img_depth, img_width, img_height = channel, 64, 64

# ---- Image input data is expressed as a 3-dime matrix of channels x width x height
input_shape = (img_depth, img_width, img_height)

# ---- Scikit-learn style dataset format from pandas dataframe (example here is sampling from dataset)
X_cnn = data_cnn.drop(["y"], axis=1, inplace=False)
y_cnn = data_cnn["y"]

# ---- A standard split of the dataset is used to evaluate and compare models
X_cnn_train, X_cnn_test, y_cnn_train, y_cnn_test = train_test_split(X_cnn, y_cnn, test_size=0.20, random_state=seed)

# ---- Create two-dim image training & test dataset
start = time()
X_train_jet_img = dataloader_cnn.create_image(X_cnn_train, 'jet', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_jet_img  = dataloader_cnn.create_image(X_cnn_test,  'jet', input_shape)

X_train_bjet_img = dataloader_cnn.create_image(X_cnn_train, 'bjet', input_shape)
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_bjet_img  = dataloader_cnn.create_image(X_cnn_test, 'bjet', input_shape)

# ---- Concatenate the various imagine depth (use axis=1 since reshape is already applied)
Images_array_train = np.concatenate((X_train_jet_img, X_train_bjet_img), axis=1)
Images_array_test  = np.concatenate((X_test_jet_img,  X_test_bjet_img),  axis=1)

# ---- Use early stopping on training when the validation loss isn't decreasing anymore
early_stopping = EarlyStopping(monitor='val_loss', patience=5)

# ---- Mixed deep learning model
mixed_model = DeepModel(input_dim=input_dim, input_shape=input_shape)
model = mixed_model.build_mixed_fn()

start = time()
model_history = model.fit([X_dnn_train, Images_array_train], y_dnn_train.values,
                          validation_split=0.20,
                          batch_size=256, epochs=80, verbose=verbose,
                          callbacks=[early_stopping])
print("Model training took %.2f seconds." % (time() - start))

# ---- Evaluate model test performance
loss, accuracy = model.evaluate([X_dnn_test, Images_array_test], y_dnn_test.values, verbose=verbose)
print('\nEvaluate test loss \n%s: %.2f%%'    % (model.metrics_names[0], loss*100))
print('Evaluete test accuracy \n%s: %.2f%%'  % (model.metrics_names[0], accuracy*100))

# ---- Evaluate model training performance 
loss, accuracy = model.evaluate([X_dnn_train, Images_array_train], y_dnn_train.values, verbose=verbose)
print('Evaluate train loss \n%s: %.2f%%'     % (model.metrics_names[0], loss*100))
print('Evaluete train accuracy \n%s: %.2f%%' % (model.metrics_names[0], accuracy*100))

# ---- Store network achitecture diagram into file
plot_model(model, to_file='plots/model_merged.pdf', show_shapes=True, show_layer_names=True)

# ---- List all data in history
print(model_history.history.keys())

## Visualize Model Training History

# ---- Summarize history for accuracy
plt.plot(model_history.history['acc'])
plt.plot(model_history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Summarize history for loss
plt.plot(model_history.history['loss'])
plt.plot(model_history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

# ---- Calculate AUC of ROC
predictions = model.predict([X_dnn_test, Images_array_test])
fpr, tpr, _ = roc_curve(y_dnn_test, predictions)
roc_auc = auc(fpr, tpr)

# Plot all ROC curves
plt.plot(fpr, tpr, lw=2, label='%s (AUC = %0.3f)'%('DNN+CNN', roc_auc))
plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title("Receiver operating characteristic curve")
leg = plt.legend(loc="best", frameon=True, fancybox=True, fontsize=8)
leg.get_frame().set_edgecolor('w')
frame = leg.get_frame()
frame.set_facecolor('White')
print('AUC: %f' % roc_auc)
plt.show()
