# ---- Import common python libraries
from __future__ import print_function
import sys
import time
import numpy as np
import pandas as pd
import random
import collections

# ---- Import from root_numpy library 
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# ---- Import from matplotlib
import matplotlib.pyplot as plt

# ----- Import scikit-learn
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import RobustScaler
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils.vis_utils import plot_model

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations

# ---- Import data loader
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel

# ---- Import optimization for hyper-parameter
from neural_network.optimization import SkOptObjective

# ---- Import plotter
from visualization.plotter import Plotter

# ---- Fix random seed for reproducibility
seed = 42
np.random.seed(seed)


# ---- Feature names
features = [
    "mass_tag_tag_min_deltaR", "median_mass_jet_jet",
    "maxDeltaEta_tag_tag", "mass_higgsLikeDijet", "HT_tags",
    "btagDiscriminatorAverage_tagged", "mass_jet_tag_min_deltaR",
    "mass_jet_jet_min_deltaR", "mass_tag_tag_max_mass", "maxDeltaEta_jet_jet",
    "centrality_jets_leps", "centrality_tags", "globalTimesEventWeight"
]

# --- Load dataset
signal_sample     = "data/signalMC.root"
background_sample = "data/backgroundMC.root"
tree_category     = "event_mvaVariables_step7_cate4"

dataloader = DataLoader(signal_sample, background_sample, tree_category, features)
data = dataloader.data
print("Total number of events: {}\nNumber of features: {}\n".format(data.shape[0], data.shape[1]))

# ---- Create features dataframe and target array
X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
y = data["y"]
input_dim = X.shape[1]

# ---- Split dataset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=seed)

# ---- Preprocessing using 0-1 scaling by removing the mean and scaling to unit variance
scaler = RobustScaler()

# Remove the y column from the correlation matrix
# after using it to select background and signal
sig = data.query("y == 1.").drop('globalTimesEventWeight', axis=1, inplace=False)
bg  = data.query("y == 0.").drop('globalTimesEventWeight', axis=1, inplace=False)

# ---- Plotting utility
plotter = Plotter()

# ---- Correlation matrix between input variables for signal
plotter.correlation_matrix(sig)
plotter.savePlots(dir='plots', filename='correlation_matrix_sig.pdf')

# ---- Correlation between input variables for background
plotter.correlation_matrix(bg)
plotter.savePlots(dir='plots', filename='correlation_matrix_bkg.pdf')

# ---- Set deep neural network architecture: [m input] -> [n neurons] -> [1 output]
dnn = DeepModel(input_dim=input_dim)

# ---- Store network architecture
plot_model(dnn.build_dnn_fn(), to_file='plots/model_dnn.pdf', show_shapes=True, show_layer_names=True)

# ---- Create model for use in scikit-learn
pipe_classifiers = {
    'kerasclassifier':  make_pipeline(scaler,  KerasClassifier(build_fn=dnn.build_dnn_fn, batch_size=256, epochs=80, verbose=2))
    }

# ---- Check for model overtraining
plotter.overfitting(pipe_classifiers['kerasclassifier'], X_train, X_test, y_train, y_test, bins=50)
plotter.savePlots(dir='plots', filename='overtraining_plot.pdf')

# ---- Hyper-parameter validation curves
name = 'KerasClassifier'.lower()

param_name = name+'__nlayers'
param_range = np.linspace(1, 10, num=10, dtype=int)

# paramter grid
params = {name.lower()+'__neurons': 12}

plotter.validation_curve(pipe_classifiers['kerasclassifier'],
                         X_train, y_train,
                         param_name,
                         params,
                         param_range,
                         cv=3, # 3-fold cross-validation
                         scoring='roc_auc', #neg_log_loss
                         logx=False,
                         n_jobs=1)
plotter.savePlots(dir='plots', filename='validation_nlayers_curve.pdf')


param_name = name+'__nneurons'
param_range = np.linspace(10, 100, num=10, dtype=int)
params = {name.lower()+'__nneurons': 1}

plotter.validation_curve(pipe_classifiers['kerasclassifier'],
                         X_train, y_train,
                         param_name,
                         params,
                         param_range,
                         cv=3, # 3-fold cross-validation
                         scoring='roc_auc', #neg_log_loss
                         logx=False,
                         n_jobs=1)
plotter.savePlots(dir="plots", filename='validation_nneurons_curve.pdf') 


param_name = name+'__dropout_rate'
param_range = np.linspace(0.01, 0.001, num=10, dtype=float)
params = {name.lower()+'__dropout_rate': 1}

plotter.validation_curve(pipe_classifiers['kerasclassifier'],
                         X_train, y_train,
                         param_name,
                         params,
                         param_range,
                         cv=3, # 3-fold cross-validation
                         scoring='roc_auc', #neg_log_loss
                         logx=False,
                         n_jobs=1)
plotter.savePlots(dir='plots', filename='validation_dropout_rate_curve.pdf')


param_name = name+'__l2_norm'
param_range = np.linspace(0.1, 0.01, num=10, dtype=float)
params = {name.lower()+'__l2_norm': 1}

plotter.validation_curve(pipe_classifiers['kerasclassifier'],
                         X_train, y_train,
                         param_name,
                         params,
                         param_range,
                         cv=3, # 3-fold cross-validation
                         scoring='roc_auc', #neg_log_loss
                         logx=False,
                         n_jobs=1)
plotter.savePlots(dir='plots', filename='validation_l2_norm_curve.pdf')


param_name = name+'__learning_rate'
param_range = np.linspace(0.001, 0.01, num=10, dtype=int)
params = {name.lower()+'__nneurons': 1}

plotter.validation_curve(pipe_classifiers['kerasclassifier'],
                         X_train, y_train,
                         param_name,
                         params,
                         param_range,
                         cv=3, # 3-fold cross-validation
                         scoring='roc_auc', #neg_log_loss
                         logx=False,
                         n_jobs=1)
plotter.savePlots(dir='plots', filename='validation_learning_rate_curve.pdf')

# ---- Draw the input variable distribuitons (signal and background separately)
plotter.signal_background(data.query("y == 0."),
                          data.query("y == 1."),
                          column=features, bins=40)
plotter.savePlots(dir='plots', filename='signal_background.pdf')

# ---- Assessing a Classifier's Performance
plotter.roc_curve(pipe_classifiers, X_train, X_test, y_train, y_test)
plotter.savePlots(dir='plots', filename='roc_curve.pdf')

# ---- Cross validation with 100 iterations to get smoother mean test and train
# score curves, each time with 20% data randomly selected as a validation set.
cv = ShuffleSplit(n_splits=3, test_size=0.20, random_state=seed)

plotter.learning_curve(pipe_classifiers['kerasclassifier'],
                       X_train, y_train,
                       ylim=(0.4, 1.01),
                       cv=cv, n_jobs=1,
                       train_sizes=np.linspace(0.1, 1.0, 10)
                       )
plotter.savePlots(dir='plots', filename='learning_curve.pdf')

# ---- Calibration curve (reliability curve)
plotter.calibration_curve(pipe_classifiers['kerasclassifier'], X, y, cv='prefit')
plotter.savePlots(dir='plots', filename='calibration_curve.pdf')

# ---- Heat map correpsonding to the neural network weights for the 1st layer per input variable
plotter.dnn_weight_map(pipe_classifiers['kerasclassifier'].named_steps['kerasclassifier'],
                       X_train.values, y_train.values,
                       features)
plotter.savePlots(dir='plots', filename='network_weights_map.pdf')

# ---- Set configuration space
parameters = collections.OrderedDict(
    [
        ('kerasclassifier__nlayers',  (1, 3)),
        ('kerasclassifier__nneurons', (150, 300)),
        ('kerasclassifier__l2_norm',  (0.01, 0.1)), 
        ('kerasclassifier__dropout_rate', (0.01, 0.1))
    ])

# ---- Instantiate objective function to optimize
estimator = pipe_classifiers['kerasclassifier']
objective = SkOptObjective(estimator, X, y)
objective.paramKeys(parameters.keys())

# ---- Bayesian optimization based on Gaussian process regression search (controlling the exploration-exploitation trade-off)
clf_gp_ei = gp_minimize(func=objective,                   # the function to minimize
                        dimensions=parameters.values(),   # the bounds on each dimension of the optimization space 
                        acq_func="EI",                    # the acquisition function ("EI", "LCB", "PI")
                        n_calls=10,                       # the number of evaluations of the objective function
                        random_state=0,                   # the random seed  
                        n_jobs=1)                         # the number of threads to use
print("Best score=%.4f (EI)" % clf_gp_ei.fun)

print("""Expected Improvement (EI) best parameters:
- nlayers= %s
- nneurons= %s
- l2_norm= %s
- dropout_rate= %s""" % (str(clf_gp_ei.x[0]), str(clf_gp_ei.x[1]),
                         str(clf_gp_ei.x[2]), str(clf_gp_ei.x[3])))

# ---- Evalution
plot_evaluations(clf_gp_ei, bins=10)
plt.show()

# ---- Convergence
plot_convergence(clf_gp_ei);
plt.show()
