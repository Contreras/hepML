# Sources:
#    - http://cv-tricks.com/tensorflow-tutorial/training-convolutional-neural-network-for-image-classification/
#    - https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/3_NeuralNetworks/convolutional_network.py
#    - https://github.com/sankit1/cv-tricks.com/blob/master/Tensorflow-tutorials/tutorial-2-image-classifier/train.py

""" Convolutional Neural Network.
Build and train a convolutional neural network with TensorFlow.
This example is using TensorFlow layers API,
example for a raw implementation with variables.
"""
from __future__ import division, print_function, absolute_import
## Create a Convolutional Neural Network with Keras

# ---- Import basic libraries
import matplotlib.pyplot as plt
import numpy as np
from time import time

# ---- Import Keras library
import keras
from keras.models import model_from_json
from keras.utils import plot_model
from keras.utils import multi_gpu_model
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from keras.layers import Input
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.merge import concatenate

# ---- Import Scikit-learn library
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, roc_auc_score

# ---- Import data loader
from neural_network.dataloaders import DataLoader

# ---- Import neural network modelling
from neural_network.models import DeepModel

# ---- Import plotter
from visualization.plotter import Plotter


# ---- Declare sample and features to use
signal_sample     = 'data/signal.root'
background_sample = 'data/background.root'
treename          = 'event_mvaVariables_step7_cate4'

features          = [
    'lepton_1_px', 'lepton_1_py', 'lepton_1_pz',
    'lepton_2_px', 'lepton_2_py', 'lepton_2_pz',
    'jet_1_px', 'jet_1_py', 'jet_1_pz',
    'jet_2_px', 'jet_2_py', 'jet_2_pz',
    'jet_3_px', 'jet_3_py', 'jet_3_pz',
    'jet_4_px', 'jet_4_py', 'jet_4_pz',
    'jet_5_px', 'jet_5_py', 'jet_5_pz',
    'jet_6_px', 'jet_6_py', 'jet_6_pz',
    ]

verbose = 2

# ---- Dimensions of our images (use 8 x 8 pixel image)
channel = 2
img_depth, img_width, img_height = channel, 64, 64

img_size = 64

# ---- Image input data is expressed as a 3-dim matrix of channels x width x height
input_shape = (img_depth, img_width, img_height)

# ---- Load the analysis dataset
dataloader =  DataLoader(signal_sample, background_sample, treename, features)
print("Total number of events: {}\nNumber of features (incl. class label): {}\n".format(dataloader.data.shape[0],
                                                                                        dataloader.data.shape[1]))

# ---- Scikit-learn style dataset format from pandas dataframe (example here is sampling from dataset)
frac=1e-2
sampled_data = dataloader.data.sample(frac=frac, replace=False, random_state=42)

X = sampled_data.drop(['y'], axis=1, inplace=False)
y = sampled_data['y']

# ---- A standard split of the dataset is used to evaluate and compare models
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)

# ---- Create two-dim image training & test dataset
start = time()
X_train_jet_img = dataloader.create_image(X_train, 'jet', input_shape, image_data_format='channels_first')
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_jet_img  = dataloader.create_image(X_test,  'jet', input_shape)

start = time()
X_train_lepton_img = dataloader.create_image(X_train, 'lepton', input_shape, image_data_format='channels_first')
print("Took %.2f seconds to transform training data into image data format."  % (time() - start))
X_test_lepton_img  = dataloader.create_image(X_test, 'lepton', input_shape)

# ---- Concatenate the various imagine depth (use axis=1 since reshape is already applied)
train_img = np.concatenate((X_train_jet_img, X_train_lepton_img), axis=1)
test_img  = np.concatenate((X_test_jet_img,  X_test_lepton_img),  axis=1)

train_labels = y_train.values
test_labels  = y_test.values


import tensorflow as tf

# Training Parameters
learning_rate = 0.001
num_steps = 2000
batch_size = 208

# Network Parameters
num_input = train_img.shape[1]  # data input img shape
num_classes = 2  # total classes (0 or 1)
dropout = 0.25   # Dropout, probability to drop a unit


# Create the neural network
def conv_net(x_dict, n_classes, dropout, reuse, is_training):

    # Define a scope for reusing the variables
    with tf.variable_scope('ConvNet', reuse=reuse):
        # TF Estimator input is a dict, in case of multiple inputs
        x = x_dict['images']

        # Data input is a 1-D vector of 784 features (64*64 pixels)
        # Reshape to match picture format [Height x Width x Channel]
        # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
        x = tf.reshape(x, shape=[-1, 64, 64, 2])

        # Convolution Layer with 32 filters and a kernel size of 5
        conv1 = tf.layers.conv2d(x, 32, 5, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        conv1 = tf.layers.max_pooling2d(conv1, 2, 2)

        # Convolution Layer with 64 filters and a kernel size of 3
        conv2 = tf.layers.conv2d(conv1, 64, 3, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        conv2 = tf.layers.max_pooling2d(conv2, 2, 2)

        # Flatten the data to a 1-D vector for the fully connected layer
        fc1 = tf.contrib.layers.flatten(conv2)

        # Fully connected layer (in tf contrib folder for now)
        fc1 = tf.layers.dense(fc1, 2) #1024
        # Apply Dropout (if is_training is False, dropout is not applied)
        fc1 = tf.layers.dropout(fc1, rate=dropout, training=is_training)

        # Output layer, class prediction
        out = tf.layers.dense(fc1, n_classes)

    return out


# Define the model function (following TF Estimator Template)
def model_fn(features, labels, mode):

    # Build the neural network
    # Because Dropout have different behavior at training and prediction time, we
    # need to create 2 distinct computation graphs that still share the same weights.
    logits_train = conv_net(features, num_classes, dropout, reuse=False,
                            is_training=True)
    logits_test = conv_net(features, num_classes, dropout, reuse=True,
                           is_training=False)

    # Predictions
    pred_classes = tf.argmax(logits_test, axis=1)
    pred_probas = tf.nn.softmax(logits_test)

    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes)

    # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits_train, labels=tf.cast(labels, dtype=tf.int32)))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op,
                                  global_step=tf.train.get_global_step())

    # Evaluate the accuracy of the model
    acc_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)

    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=pred_classes,
        loss=loss_op,
        train_op=train_op,
        eval_metric_ops={'accuracy': acc_op})

    return estim_specs

# Build the Estimator
model = tf.estimator.Estimator(model_fn)

# Define the input function for training
input_fn = tf.estimator.inputs.numpy_input_fn(
    x={'images': train_img }, 
    y=train_labels,
    batch_size=batch_size, num_epochs=None, shuffle=True)

# Train the Model
model.train(input_fn, steps=num_steps)

# Evaluate the Model

# Define the input function for evaluating
input_fn = tf.estimator.inputs.numpy_input_fn(
    x={'images': test_img }, 
    y=test_labels,
    batch_size=batch_size, shuffle=False)

# Use the Estimator 'evaluate' method
e = model.evaluate(input_fn)

print("Testing Accuracy:", e['accuracy'])
